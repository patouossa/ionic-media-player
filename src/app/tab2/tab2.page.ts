import { Component } from '@angular/core';

declare var MediaRecorder: any;
declare var navigator: any;
@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss', '../../assets/styles/styles.css']
})
export class Tab2Page {

    recordButton: any;
    stopButton: any;
    liveStream: any;
    recorder: any
    liveVideo: any
    ngAfterViewInit() {
        this.recordButton = document.getElementById('record');
        this.stopButton = document.getElementById('stop');

        navigator.getUserMedia = (navigator.getUserMedia ||
            navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia ||
            navigator.msGetUserMedia);
        // get video & audio stream from user
        navigator.mediaDevices.getUserMedia(
            {
                audio: true,
                video: true
            })
            .then(
            (stream) => {
                this.liveStream = stream;

                this.liveVideo = document.getElementById('live');
                const binaryData = [];
                binaryData.push(this.liveStream);
                //this.liveVideo.src = URL.createObjectURL(new Blob(binaryData, { type: "video/mp4" }));
                this.liveVideo.srcObject = stream; //URL.createObjectURL(new Blob(binaryData, { type: "video/mp4" }));
                this.liveVideo.play();

                this.recordButton.disabled = false;
                this.recordButton.addEventListener('click', () => { this.startRecording(this) });
                this.stopButton.addEventListener('click', () => { this.stopRecording(this) });
                
            },
            // Error callback
            function (err) {
                console.log('The following gUM error occured: ' + err);
            }
        );
    }

    startRecording($this) {
        console.log($this.liveStream);
        $this.recorder = new MediaRecorder($this.liveStream);

        $this.recorder.addEventListener('dataavailable', $this.onRecordingReady);

        $this.recordButton.disabled = true;
        $this.stopButton.disabled = false;
        $this.recorder.start();
    }

    stopRecording($this) {
        $this.recordButton.disabled = false;
        $this.stopButton.disabled = true;

        // Stopping the recorder will eventually trigger the 'dataavailable' event and we can complete the recording process
        $this.recorder.stop();
    }

    onRecordingReady(e) {
        const video: any = document.getElementById('recording');
        // e.data contains a blob representing the recording

        const binaryData = [];
        binaryData.push(e.data);
        video.src = URL.createObjectURL(new Blob(binaryData, { type: "video/mp4" }));
        video.play();
    }
}
