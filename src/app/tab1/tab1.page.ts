import { Component, AfterViewInit } from '@angular/core';
import {any} from 'codelyzer/util/function';
// import * as videoplay from '../../assets/scripts/app.js';

declare var MediaStreamTrack: any;
declare var navigator: any;
declare var window: any;
declare var webkitAudioContext: any;
declare var audioCtx: any;
declare var MediaRecorder: any;
declare var evtTgt: any;
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss', '../../assets/styles/app.css', '../../assets/styles/normalize.css']
})
export class Tab1Page {
    public constraints = {};
    $this = this;


    // fork getUserMedia for multiple browser versions, for the future
// when more browsers support MediaRecorder
    // getUserMedia;
    /*navigator.getUserMedia: any = (navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia ||
        navigator.msGetUserMedia);*/

    // set up basic variables for app

    record: any; // = document.querySelector('.record');
    stop: any; // = document.querySelector('.stop');
    soundClips: any; // = document.querySelector('.sound-clips');
    canvas: any; // = document.querySelector('.visualizer');

    // visualiser setup - create web audio api context and canvas

    canvasCtx: any; // = this.canvas.getContext('2d');

    // main block for doing the audio recording
    constructor() {

    }
    ngAfterViewInit() {
        navigator.getUserMedia = (navigator.getUserMedia ||
            navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia ||
            navigator.msGetUserMedia);
        // Put here the code you want to execute
        this.record = document.querySelector('.record');
        this.stop = document.querySelector('.stop');
        this.soundClips = document.querySelector('.sound-clips');
        this.canvas = document.getElementById('visualizer');
        console.log(this.canvas);
        console.log(document.getElementById('visualizer'));
        this.canvasCtx = this.canvas.getContext('2d');
        if (navigator.getUserMedia) {
            console.log('getUserMedia supported.');
            navigator.getUserMedia(
                // constraints - only audio needed for this app
                {
                    audio: true,
                    video: true
                },

                // Success callback
                function (stream) {


                    function visualize(stream_) {
                        const audioCtx_: any  = new (window.AudioContext || webkitAudioContext)();
                        const source = audioCtx_.createMediaStreamSource(stream_);

                        const canvas: any = document.getElementById('visualizer');
                        const canvasCtx: any = canvas.getContext('2d');
                        const analyser = audioCtx_.createAnalyser();
                        analyser.fftSize = 2048;
                        const bufferLength = analyser.frequencyBinCount;
                        const dataArray = new Uint8Array(bufferLength);

                        source.connect(analyser);
                        // analyser.connect(audioCtx.destination);

                        const WIDTH = canvas.width;
                        const HEIGHT = canvas.height;

                        draw();

                        function draw() {

                            requestAnimationFrame(draw);

                            analyser.getByteTimeDomainData(dataArray);

                            canvasCtx.fillStyle = 'rgb(200, 200, 200)';
                            canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);

                            canvasCtx.lineWidth = 2;
                            canvasCtx.strokeStyle = 'rgb(0, 0, 0)';

                            canvasCtx.beginPath();

                            const sliceWidth = WIDTH * 1.0 / bufferLength;
                            let x = 0;


                            for (let i = 0; i < bufferLength; i++) {

                                const v = dataArray[i] / 128.0;
                                const y = v * HEIGHT / 2;

                                if ( i === 0) {
                                    canvasCtx.moveTo(x, y);
                                } else {
                                    canvasCtx.lineTo(x, y);
                                }

                                x += sliceWidth;
                            }

                            canvasCtx.lineTo(canvas.width, canvas.height / 2);
                            canvasCtx.stroke();

                        }
                    }

                    const options = {mimeType: 'video/webm; codecs=vp9'};
                    const mediaRecorder = new MediaRecorder(stream, options);
                    const record: any = document.querySelector('.record');
                    const stop: any = document.querySelector('.stop');
                    const soundClips: any = document.querySelector('.sound-clips');
                    console.log('hello world');
                    visualize(stream);

                    record.onclick = function () {
                        mediaRecorder.start();
                        console.log(mediaRecorder.state);
                        console.log('recorder started');
                        record.style.background = 'red';
                        record.style.color = 'black';
                    };

                    stop.onclick = function () {
                        mediaRecorder.stop();
                        console.log(mediaRecorder.state);
                        console.log('recorder stopped');
                        record.style.background = '';
                        record.style.color = '';
                    };

                    mediaRecorder.ondataavailable = function (e) {
                        console.log('data available');

                        const clipName = prompt('Enter a name for your sound clip');

                        const clipContainer = document.createElement('article');
                        const clipLabel = document.createElement('p');
                        const audio: any = document.createElement('audio');
                        const deleteButton = document.createElement('button');

                        clipContainer.classList.add('clip');
                        audio.setAttribute('controls', '');
                        deleteButton.innerHTML = 'Delete';
                        clipLabel.innerHTML = clipName;

                        clipContainer.appendChild(audio);
                        clipContainer.appendChild(clipLabel);
                        clipContainer.appendChild(deleteButton);
                        soundClips.appendChild(clipContainer);

                        const audioURL = window.URL.createObjectURL(e.data);
                        audio.src = audioURL;

                        deleteButton.onclick = function (event) {
                            evtTgt = event.target;
                            evtTgt.parentNode.parentNode.removeChild(evtTgt.parentNode);
                        };
                    };
                },

                // Error callback
                function (err) {
                    console.log('The following gUM error occured: ' + err);
                }
            );
        } else {
            console.log('getUserMedia not supported on your browser!');
        }
    }




}
